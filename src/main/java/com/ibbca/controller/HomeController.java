package com.ibbca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ibbca.entity.Nasabah;
import com.ibbca.service.NasabahService;

@Controller
public class HomeController {
	
	NasabahService nasabahService;
	
	@Autowired
	public void setNasabahService(NasabahService nasabahService) {
		this.nasabahService = nasabahService;
	}

	@RequestMapping("/home")
	public String home(Model model, HttpServletRequest request) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDetails userDetail = (UserDetails) auth.getPrincipal();
		String username = userDetail.getUsername();
		Nasabah nasabah= this.nasabahService.getNasabahByUsername(username);
		String nama =  nasabah.getNama();
		int id =  nasabah.getId();
		
		model.addAttribute("nama", nama);
		model.addAttribute("id", id);
		request.getSession().setAttribute("nama", nama);
		request.getSession().setAttribute("id", id);

		String bahasa = (String)request.getSession().getAttribute("bahasa");
		if(bahasa==null) {
			request.getSession().setAttribute("bahasa", "ID");
		}
		if("ID".equals(bahasa)) {
			return "home_id";
		}else{
			return "home_en";
		}
	}
	
	@RequestMapping("/ganti-bahasa")
	public String setLanguagePage(HttpServletRequest request) {
		String bahasa = (String)request.getSession().getAttribute("bahasa");
		if("ID".equals(bahasa)) {
			return "set_language_id";
		}else{
			return "set_language_en";
		}
	}
	
	@RequestMapping(value="/ganti-bahasa/save", method = RequestMethod.POST)
	public String setLanguage(HttpServletRequest request, RedirectAttributes attr) {
		request.getSession().setAttribute("bahasa", (String)request.getParameter("bahasa"));
		
		if("ID".equals((String)request.getParameter("bahasa"))) {
			attr.addFlashAttribute("message", "Bahasa telah diganti ke Indonesia");
		}else {
			attr.addFlashAttribute("message", "Language has been set to English");
		}
		
		return "redirect:/ganti-bahasa";
	}
	
	@RequestMapping(value = "/login", method = { RequestMethod.GET, RequestMethod.POST })
	public String login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, Model model) {
		String error_message = null;
		String logout_message = null;
		if (error != null) {
			error_message = "Username atau password salah!";
		}
		if (logout != null) {
			logout_message = "Logout berhasil!";
		}
		model.addAttribute("error_message", error_message);
		model.addAttribute("logout_message", logout_message);
		return "login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout=true";
	}

	
	@RequestMapping("/denied")
	public String denied(Model model) {
		return "denied";
	}
}
