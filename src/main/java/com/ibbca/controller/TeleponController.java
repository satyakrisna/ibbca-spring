package com.ibbca.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ibbca.entity.Telepon;
import com.ibbca.service.TeleponService;

@Controller
public class TeleponController {

	TeleponService teleponService;

	@Autowired
	public void setTeleponService(TeleponService teleponService) {
		this.teleponService = teleponService;
	}
	
	@RequestMapping(value="/telepon", method = RequestMethod.GET)
	public String addTelepon(Model model, HttpServletRequest request) {
		int id_nasabah= (int)request.getSession().getAttribute("id");
		model.addAttribute("id_nasabah", id_nasabah);
		model.addAttribute("telepon", new Telepon());
		
		String bahasa = (String)request.getSession().getAttribute("bahasa");
		if("ID".equals(bahasa)) {
			return "telepon_id";
		}else{
			return "telepon_en";
		}
	}
	
	@RequestMapping(value="/telepon/save", method = RequestMethod.POST)
	public String saveTelepon(@ModelAttribute("telepon") Telepon t, RedirectAttributes attr) {
		this.teleponService.saveTelepon(t);
		attr.addFlashAttribute("message", "success");
		attr.addFlashAttribute("provider", t.getProvider_telepon());
		attr.addFlashAttribute("nomor", t.getNomor_telepon());
		attr.addFlashAttribute("nominal", t.getNominal_telepon());
		return "redirect:/telepon";
	}
}
