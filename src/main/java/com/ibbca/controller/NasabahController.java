package com.ibbca.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ibbca.service.NasabahService;

@Controller
public class NasabahController {

	NasabahService nasabahService;

	@Autowired
	public void setNasabahService(NasabahService nasabahService) {
		this.nasabahService = nasabahService;
	}
	
	@RequestMapping(value="/ganti-password", method= RequestMethod.GET)
	public String changePasswordPage(Model model, HttpServletRequest request) {
		String bahasa = (String)request.getSession().getAttribute("bahasa");
		if("ID".equals(bahasa)) {
			return "change_password_id";
		}else{
			return "change_password_en";
		}
	}
	
	@RequestMapping(value="ganti-password/save", method = RequestMethod.POST)
	public String changePassword(HttpServletRequest request, RedirectAttributes attr) {
		int id=(int)request.getSession().getAttribute("id");
		String oldPassword = (String)request.getParameter("oldPassword");
		String newPassword = (String)request.getParameter("newPassword");
		if("success".equals(this.nasabahService.updatePassword(id, oldPassword, newPassword))){
			attr.addFlashAttribute("success", "Password berhasil diganti!");
		}else {
			attr.addFlashAttribute("error", "Password lama salah!");
		}
		return "redirect:/ganti-password";
	}
}
