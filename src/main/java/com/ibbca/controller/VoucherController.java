package com.ibbca.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ibbca.entity.Voucher;
import com.ibbca.service.VoucherService;

@Controller
public class VoucherController {

	VoucherService voucherService;

	@Autowired
	public void setVoucherService(VoucherService voucherService) {
		this.voucherService = voucherService;
	}
	
	@RequestMapping(value="/voucher", method = RequestMethod.GET)
	public String addVoucher(Model model, HttpServletRequest request) {
		int id_nasabah= (int)request.getSession().getAttribute("id");
		model.addAttribute("id_nasabah", id_nasabah);
		model.addAttribute("voucher", new Voucher());
		
		String bahasa = (String)request.getSession().getAttribute("bahasa");
		if("ID".equals(bahasa)) {
			return "voucher_id";
		}else{
			return "voucher_en";
		}
	}
	
	@RequestMapping(value="/voucher/save", method = RequestMethod.POST)
	public String saveVoucher(@ModelAttribute("voucher") Voucher v, RedirectAttributes attr) {
		this.voucherService.saveVoucher(v);
		attr.addFlashAttribute("message", "success");
		attr.addFlashAttribute("provider", v.getProvider_voucher());
		attr.addFlashAttribute("nomor", v.getNomor_telepon());
		attr.addFlashAttribute("nominal", v.getNominal_voucher());
		attr.addFlashAttribute("token", v.getToken_voucher());
		return "redirect:/voucher";
	}
}
