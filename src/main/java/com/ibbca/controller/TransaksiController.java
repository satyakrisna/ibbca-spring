package com.ibbca.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibbca.entity.Transaksi;
import com.ibbca.service.TransaksiService;

@Controller
public class TransaksiController {

	private TransaksiService transaksiService;
	
	@Autowired
	public void setTransaksiService(TransaksiService ts){
		this.transaksiService = ts;
	}
	
	@RequestMapping("/transaksi")
	public String listTransaksi(Model model, HttpServletRequest request) {
		int id_nasabah= (int)request.getSession().getAttribute("id");
		model.addAttribute("transaksi", new Transaksi());
		model.addAttribute("listTransaksi", this.transaksiService.listTransaksi(id_nasabah));
		
		String bahasa = (String)request.getSession().getAttribute("bahasa");
		if("ID".equals(bahasa)) {
			return "transaksi_id";
		}else{
			return "transaksi_en";
		}
	}
}
