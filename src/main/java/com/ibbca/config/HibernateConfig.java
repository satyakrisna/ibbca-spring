package com.ibbca.config;


import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.HibernateException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@Configuration
public class HibernateConfig {

	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl("jdbc:postgresql://127.0.0.1:5432/springweb");
		dataSource.setUsername("postgres");
		dataSource.setPassword("admin");
		return dataSource;
	}
	
	@Bean
    public LocalSessionFactoryBean getSessionFactory() {
        
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        try {
            factoryBean.setDataSource(this.dataSource());
            factoryBean.setPackagesToScan("com.ibbca.entity");
            
            Properties props = new Properties();
            props.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
            props.setProperty("javax.persistence.validation.mode", "none");
            props.setProperty("hibernate.hbm2ddl.auto", "update");
            
            factoryBean.setHibernateProperties(props);
        }catch(HibernateException e) {
        	e.printStackTrace();
        }
        return factoryBean;
    }
	
	@Bean
    public HibernateTransactionManager getTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(getSessionFactory().getObject());
        return transactionManager;
    }
}
