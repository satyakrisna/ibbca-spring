package com.ibbca.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = { "com.ibbca.service","com.ibbca.dao"})
@EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		System.out.println("Security mantul");
		http
	  	.formLogin()
	  		.loginPage("/login")
	  		.defaultSuccessUrl("/home", true)
	  		.failureUrl("/login?error")
			.permitAll()
			.and()
		.exceptionHandling()
			.accessDeniedPage("/denied")
			.and()
		.authorizeRequests()
			.antMatchers("/home/**").permitAll()
			//.antMatchers("/accounts/edit*").hasRole("EDITOR")
			//.antMatchers("/accounts/account*").hasAnyRole("VIEWER", "EDITOR")
			.antMatchers("/tagihan/**").authenticated()
			.antMatchers("/telepon/**").authenticated()
			.antMatchers("/transaksi/**").authenticated()
			.antMatchers("/voucher/**").authenticated()
			.antMatchers("/home/**").authenticated()
			.antMatchers("/ganti-password/**").authenticated()
			.and()
		.logout()
			.logoutSuccessUrl("/login?logout")
			.permitAll()
			.and();
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		//auth.inMemoryAuthentication().withUser("user")
//				.password(passwordEncoder().encode("user")).roles("USER").and().withUser("admin")
//				.password(passwordEncoder().encode("admin")).roles("USER", "ADMIN");

	}

	
}
