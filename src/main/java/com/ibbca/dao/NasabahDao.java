package com.ibbca.dao;

import com.ibbca.entity.Nasabah;

public interface NasabahDao {

	public Nasabah getNasabahByUsername(String username);
	public Nasabah getNasabah(int id);
	public String updatePassword(int id, String oldPassword, String newPassword);
}
