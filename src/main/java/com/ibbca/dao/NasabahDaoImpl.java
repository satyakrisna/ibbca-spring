package com.ibbca.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.ibbca.entity.Nasabah;

@Repository
public class NasabahDaoImpl implements NasabahDao{

	SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Nasabah getNasabahByUsername(String username_nasabah) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Nasabah> nasabahList = session.createQuery("from Nasabah where username='"+username_nasabah+"'").list();
		return nasabahList.get(0);
	}
	
	@Override
	public Nasabah getNasabah(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Nasabah nasabah = currentSession.get(Nasabah.class, id);
		return nasabah;
	}
	
	@Override
	public String updatePassword(int id, String oldPassword, String newPassword) {
		Session session = sessionFactory.getCurrentSession();
		Nasabah nasabah = session.byId(Nasabah.class).load(id);
		newPassword = new BCryptPasswordEncoder().encode(newPassword);
		if(new BCryptPasswordEncoder().matches(oldPassword, nasabah.getPassword())) {
			nasabah.setPassword(newPassword);
			session.saveOrUpdate("password", nasabah);
			return "success";
		}else {
			return "fail";
		}
	}
	
}
