package com.ibbca.dao;

import java.util.List;

import com.ibbca.entity.Transaksi;

public interface TransaksiDao {

	public List<Transaksi> listTransaksi(int id_nasabah);
}
