package com.ibbca.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibbca.entity.Voucher;

@Repository
public class VoucherDaoImpl implements VoucherDao{

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void saveVoucher(Voucher v) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(v);
	}

	

}
