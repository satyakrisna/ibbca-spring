package com.ibbca.dao;

import java.util.List;

import com.ibbca.entity.Tagihan;
import com.ibbca.entity.Transaksi;

public interface TagihanDao {

	public void updateTagihan(Tagihan t);
	public List<Transaksi> listTransaksi(int id_nasabah);
	public Tagihan getTagihan(int id_nasabah);
}
