package com.ibbca.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ibbca.entity.Telepon;

@Repository
public class TeleponDaoImpl implements TeleponDao{

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public void saveTelepon(Telepon t) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(t);
	}

}
