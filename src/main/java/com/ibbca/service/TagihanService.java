package com.ibbca.service;

import java.util.List;

import com.ibbca.entity.Tagihan;
import com.ibbca.entity.Transaksi;

public interface TagihanService {

	public void updateTagihan(Tagihan t);
	public List<Transaksi> listTransaksi(int id_nasabah);
	public Tagihan getTagihan(int id_nasabah);
}
