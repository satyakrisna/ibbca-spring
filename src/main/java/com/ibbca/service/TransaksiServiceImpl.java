package com.ibbca.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.ibbca.dao.TransaksiDao;
import com.ibbca.entity.Transaksi;

@Service
@EnableTransactionManagement
public class TransaksiServiceImpl implements TransaksiService{

	private TransaksiDao transaksiDao;

	@Autowired
	public void setTransaksiDao(TransaksiDao transaksiDao) {
		this.transaksiDao = transaksiDao;
	}
	
	@Override
	@Transactional
	public List<Transaksi> listTransaksi(int id_nasabah) {
		return this.transaksiDao.listTransaksi(id_nasabah);
	}
}
