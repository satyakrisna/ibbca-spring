package com.ibbca.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.ibbca.dao.VoucherDao;
import com.ibbca.entity.Voucher;

@Service
@EnableTransactionManagement
public class VoucherServiceImpl implements VoucherService{

	VoucherDao voucherDao;
	
	@Autowired
	public void setVoucherDao(VoucherDao voucherDao) {
		this.voucherDao = voucherDao;
	}

	@Override
	@Transactional
	public void saveVoucher(Voucher v) {
		this.voucherDao.saveVoucher(v);
	}

}
