package com.ibbca.service;

import java.util.List;

import com.ibbca.entity.Transaksi;

public interface TransaksiService {

	public List<Transaksi> listTransaksi(int id_nasabah);
}
