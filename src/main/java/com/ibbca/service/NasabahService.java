package com.ibbca.service;

import com.ibbca.entity.Nasabah;

public interface NasabahService {

	public Nasabah getNasabahByUsername(String username);
	public Nasabah getNasabah(int id);
	public String updatePassword(int id, String oldPassword, String newPassword);
}
