package com.ibbca.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibbca.dao.NasabahDao;
import com.ibbca.entity.Nasabah;

@Service
public class NasabahServiceImpl implements NasabahService, UserDetailsService{

	NasabahDao nasabahDao;
	
	@Autowired
	public void setNasabahDao(NasabahDao nasabahDao) {
		this.nasabahDao = nasabahDao;
	}

	@Override
	@Transactional
	public Nasabah getNasabahByUsername(String username) {
		return this.nasabahDao.getNasabahByUsername(username);
	}

	@Override
	@Transactional
	public Nasabah getNasabah(int id) {
		return this.nasabahDao.getNasabah(id);
	}

	@Override
	@Transactional
	public String updatePassword(int id, String oldPassword, String newPassword) {
		return this.nasabahDao.updatePassword(id, oldPassword, newPassword);
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Nasabah user = nasabahDao.getNasabahByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException("Bad credentials");
		}

		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		grantedAuthorities.add(new SimpleGrantedAuthority("USER"));
		grantedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));
		
		org.springframework.security.core.userdetails.User usersecurity = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				grantedAuthorities);
		System.out.println(usersecurity.getPassword());
		System.out.println(usersecurity.getUsername());
		
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				grantedAuthorities);
		
		

	}

	
	
	

}
