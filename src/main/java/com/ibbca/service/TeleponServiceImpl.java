package com.ibbca.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.ibbca.dao.TeleponDao;
import com.ibbca.entity.Telepon;

@Service
@EnableTransactionManagement
public class TeleponServiceImpl implements TeleponService{

	TeleponDao teleponDao; 
	
	@Autowired
	public void setTeleponDao(TeleponDao teleponDao) {
		this.teleponDao = teleponDao;
	}

	@Override
	@Transactional
	public void saveTelepon(Telepon t) {
		this.teleponDao.saveTelepon(t);
	}

	
}
