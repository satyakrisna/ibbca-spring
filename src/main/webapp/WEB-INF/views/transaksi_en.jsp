<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<%@ page isELIgnored="false" %>
<title>I-Banking BCA - Transaksi</title>
<meta charset="ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="<c:url value="/resources/css/content.css"/>">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="home">Internet Banking <b>BCA</b></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a><span class="glyphicon glyphicon-user"></span> Hi,
						${nama}</a></li>
				<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span>
						Logout</a></li>
			</ul>
		</div>
	</nav>
	<div class="wrapper">
		<!-- Sidebar -->
		<div id="sidebar">
			<ul class="list-unstyled">
				<li><a href="#"><span
						class="glyphicon glyphicon-home"></span> Home</a></li>
				<li><a href="#pembelian" data-toggle="collapse"
					aria-expanded="false" class="dropdown-toggle"><span
						class="glyphicon glyphicon-tags"></span> Purchase<span
						class="glyphicon glyphicon-triangle-bottom pull-right"></span></a>
					<ul class="collapse list-unstyled" id="pembelian">
						<li><a href="voucher"><span class="glyphicon glyphicon-phone"></span>
								Voucher</a></li>
					</ul></li>
				<li><a href="#pembayaran" data-toggle="collapse"
					aria-expanded="false" class="dropdown-toggle"><span
						class="glyphicon glyphicon-usd"></span> Payment<span
						class="glyphicon glyphicon-triangle-bottom pull-right"></span></a>
					<ul class="collapse list-unstyled" id="pembayaran">
						<li><a href="telepon"><span
								class="glyphicon glyphicon-earphone"></span>  Phone</a></li>
					</ul></li>
				<li class="active"><a href="#kredit" data-toggle="collapse"
					aria-expanded="false" class="dropdown-toggle"><span
						class="glyphicon glyphicon-credit-card"></span> Credit Card<span
						class="glyphicon glyphicon-triangle-bottom pull-right"></span></a>
					<ul class="collapse list-unstyled" id="kredit">
						<li class="active"><a href="transaksi"><span
								class="glyphicon glyphicon-transfer"></span> Transaction</a></li>
						<li><a href="tagihan"><span
								class="glyphicon glyphicon-duplicate"></span> Bill</a></li>
					</ul></li>
				<li><a href="#administrasi" data-toggle="collapse"
					aria-expanded="false" class="dropdown-toggle"><span
						class="glyphicon glyphicon-log-out"></span> Administration<span
						class="glyphicon glyphicon-triangle-bottom pull-right"></span></a>
					<ul class="collapse list-unstyled" id="administrasi">
						<li><a href="ganti-bahasa"><span
								class="glyphicon glyphicon-education"></span> Set Language</a></li>
						<li><a href="ganti-password"><span class="glyphicon glyphicon-user"></span>
								Change Password</a></li>
					</ul></li>
			</ul>
		</div>
		<div class="container">
			<div class="content">
				<h3>Transaction List</h3>
				<hr style="color: blue">
					<c:if test="${!empty listTransaksi}">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Shop</th>
								<th>Amount</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							
							<c:forEach items="${listTransaksi}" var="transaksi">
								<tr>
									<td>${transaksi.nama_toko}</td>
									<td><fmt:formatNumber pattern="#,###,###,###" value="${transaksi.nominal_transaksi}" /></td>
									<td><fmt:formatDate value="${transaksi.tanggal_transaksi}" pattern="dd MMM YYYY" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					</c:if>
			</div>
		</div>

	</div>
</body>
</html>