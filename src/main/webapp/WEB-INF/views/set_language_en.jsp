<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<%@ page isELIgnored="false"%>
<title>I-Banking BCA - Tagihan</title>
<meta charset="ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="<c:url value="/resources/css/content.css"/>">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="home">Internet Banking <b>BCA</b></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a><span class="glyphicon glyphicon-user"></span> Hi,
						${nama}</a></li>
				<li><a href="logout"><span
						class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			</ul>
		</div>
	</nav>
	<div class="wrapper">
		<!-- Sidebar -->
		<div id="sidebar">
			<ul class="list-unstyled">
				<li><a href="home"><span class="glyphicon glyphicon-home"></span>
						Home</a></li>
				<li><a href="#pembelian" data-toggle="collapse"
					aria-expanded="false" class="dropdown-toggle"><span
						class="glyphicon glyphicon-tags"></span> Purchase<span
						class="glyphicon glyphicon-triangle-bottom pull-right"></span></a>
					<ul class="collapse list-unstyled" id="pembelian">
						<li><a href="voucher"><span
								class="glyphicon glyphicon-phone"></span> Voucher</a></li>
					</ul></li>
				<li><a href="#pembayaran" data-toggle="collapse"
					aria-expanded="false" class="dropdown-toggle"><span
						class="glyphicon glyphicon-usd"></span> Payment<span
						class="glyphicon glyphicon-triangle-bottom pull-right"></span></a>
					<ul class="collapse list-unstyled" id="pembayaran">
						<li><a href="telepon"><span
								class="glyphicon glyphicon-earphone"></span>  Phone</a></li>
					</ul></li>
				<li class="active"><a href="#kredit" data-toggle="collapse"
					aria-expanded="false" class="dropdown-toggle"><span
						class="glyphicon glyphicon-credit-card"></span> Credit Card<span
						class="glyphicon glyphicon-triangle-bottom pull-right"></span></a>
					<ul class="collapse list-unstyled" id="kredit">
						<li><a href="transaksi"><span
								class="glyphicon glyphicon-transfer"></span> Transaction</a></li>
						<li><a href="tagihan"><span
								class="glyphicon glyphicon-duplicate"></span> Bill</a></li>
					</ul></li>
				<li><a href="#administrasi" data-toggle="collapse"
					aria-expanded="false" class="dropdown-toggle"><span
						class="glyphicon glyphicon-log-out"></span> Administration<span
						class="glyphicon glyphicon-triangle-bottom pull-right"></span></a>
					<ul class="collapse list-unstyled" id="administrasi">
						<li><a href="#"><span
								class="glyphicon glyphicon-education"></span> Set Language</a></li>
						<li class="active"><a href="ganti-password"><span
								class="glyphicon glyphicon-user"></span> Change Password</a></li>
					</ul></li>
			</ul>
		</div>
		<div class="container">
			<div class="content">
				<h3>Change Language</h3>
				<hr style="color: blue">
				<c:if test="${null != message}">
					<div class="alert alert-success" style="border-radius: 0px">
						<strong>${message }</strong>
					</div>
				</c:if>
				<form action="ganti-bahasa/save" class="form-horizontal"
					method="post">
					<div class="form-group">
						<label class="control-label col-sm-2" for="sel1">Choose Language:</label> 
						<div class="col-sm-10">
						<select
							class="form-control" id="sel1" name="bahasa">
							<option value="ID">Indonesia</option>
							<option value="EN">English</option>
						</select>
						<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success btn-flat" id="myBtn">Save</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>